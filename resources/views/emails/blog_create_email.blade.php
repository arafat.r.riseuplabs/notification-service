<!DOCTYPE html>
<html>

<head>
    <title>{{ $details['subject'] ?? 'Subject is here' }}</title>
</head>

<body>
    <p>{{ $details['body'] ?? 'Body is here' }}</p>
    <a href="{{ $details['actionUrl'] ?? url('/') }}">{{ $details['actionText'] ?? 'Action' }}</a>
    <p>Thank you for crteating blog!</p>
</body>

</html>
