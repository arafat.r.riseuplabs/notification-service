<?php

namespace App\Http\Controllers;

use App\Mail\BlogCreateMail;
use App\Notifications\BlogCreateNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class NotificationController extends Controller
{
    public function notify(Request $request)
    {
        $email = "atrahmanbd5@gmail.com";
        $details = [
            'subject' => 'Test Email',
            'body' => 'This is a test email.',
            'actionText' => 'View Details',
            'actionUrl' => url('/'),
        ];

        // Mail::to($email)->send(new BlogCreateMail($details));
        Notification::route('mail', $email)->notify(new BlogCreateNotification($details));

        return successResponse('Email sent successfully.');
    }
}
