<?php

namespace App\Http\Middleware;

use App\Services\TokenValidationService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if (!(new TokenValidationService)->validateToken($request)) {
            return abort(401, 'Unauthorized');
        }
        return $next($request);
    }
}
