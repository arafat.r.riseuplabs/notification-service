<?php

namespace App\Services;

use App\Enums\ServicesEnum;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Exception\RequestException;

class TokenValidationService
{
    protected $header;

    public function __construct()
    {
        $this->header = request()->header();
    }

    public function validateToken($request)
    {
        try {

            $response = Http::withHeaders($this->header)->get(ServicesEnum::AUTH_SERVICE_VALIDATE_AUTHENTICATION_API);
            if ($response->status() == 200) {
                return true;
            }
            return false;
        } catch (RequestException $e) {
            return false;
        }
    }
}
