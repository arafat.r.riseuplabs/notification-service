<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Builder::macro('withSlug', function (string $sourceAttributeName = 'title', string $slugAttributeName = 'slug') {
            /** @var \Illuminate\Database\Eloquent\Model $this */
            $model = $this->getModel();

            $generator = function ($model, $count = 0) use ($sourceAttributeName, $slugAttributeName, &$generator) {
                $slug = Str::slug($model->getAttribute($sourceAttributeName));
                $slug = $count === 0 ? $slug : $slug . '-' . $count;
                $exists = $model->where($slugAttributeName, $slug)->exists();
                return $exists ? $generator($model, $count + 1) : $slug;
            };

            $model::creating(function ($model) use ($slugAttributeName, $generator) {
                $model->setAttribute($slugAttributeName, $generator($model));
            });

            return $this;
        });
    }
}
