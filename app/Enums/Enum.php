<?php

namespace App\Enums;

use Illuminate\Support\Collection;
use ReflectionClass;

abstract class Enum
{
    /**
     * Reflection class instance.
     *
     * @var ReflectionClass
     */
    protected static $instance = null;

    /**
     * Constants cache.
     *
     * @var array
     */
    protected static $constantsCache = [];

    /**
     * Get reflection object.
     *
     * @return ReflectionClass
     */
    public static function getRefObj()
    {
        if (!self::$instance) {
            self::$instance = new ReflectionClass(static::class);
        }

        return self::$instance;
    }

    /**
     * Get constants.
     *
     * @return Illuminate\Support\Collection
     */
    public static function all()
    {
        if (!self::$constantsCache) {
            $constants = self::getRefObj()->getConstants();
            self::$constantsCache = new Collection($constants);
        }

        return self::$constantsCache;
    }

    /**
     * Find by key.
     *
     * @param string $key
     * @return mixed
     */
    public static function find($key)
    {
        return self::all()->get($key);
    }
}
